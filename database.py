import sqlite3
from datetime import datetime, timedelta
import random
import os
import rsa
numRecords = 10
dbFile = os.path.join(os.getcwd(), 'database.db')
tableName = 'data'
minTemp = 20.0
maxTemp = 30.0
startDate = '2024-03-17 00:00:00'
period = 300

def generate_keys():
    # Check if keys already exist
    if not os.path.exists('public.pem') or not os.path.exists('private.pem'):
        # Generate new keys
        public_key, private_key = rsa.newkeys(256)
        # Save keys to files
        with open('public.pem', 'wb') as public_file:
            public_file.write(public_key.save_pkcs1())
        with open('private.pem', 'wb') as private_file:
            private_file.write(private_key.save_pkcs1())

#generate_keys()

conn = sqlite3.connect(dbFile) #connect na database.db
cursor = conn.cursor()
 
createQuery = f"""
CREATE TABLE IF NOT EXISTS {tableName} (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    timestamp TEXT NOT NULL,
    temperature REAL NOT NULL,
    humidity REAL NOT NULL,
    air REAL NOT NULL
);
"""

cursor.execute(createQuery) #vytvoření table pokud neexistuje

users = f"""
CREATE TABLE IF NOT EXISTS users (
  user_id         INTEGER PRIMARY KEY AUTOINCREMENT,
  email            VARCHAR(100) NOT NULL,
  password             BLOB(300) NOT NULL
);
"""
cursor.execute(users) 

countQuery = f"SELECT COUNT(*) FROM {tableName}"
cursor.execute(countQuery)
count = cursor.fetchone()[0]


if count == 0:
    startTime = datetime.strptime(startDate, '%Y-%m-%d %H:%M:%S')                       #pokud je id 0 začnu timestamp od 0
else:
    maxTimestampQuery = f"SELECT MAX(timestamp) FROM {tableName}"                       #generuje od posledního timestamp
    cursor.execute(maxTimestampQuery)
    maxTimestamp = cursor.fetchone()[0]
    maxTimestampDatetime = datetime.strptime(maxTimestamp, '%Y-%m-%d %H:%M:%S')
    startTime = maxTimestampDatetime + timedelta(seconds=period)
 
for i in range(numRecords):                                                             #geneneruju 10 hodnot
    timestamp = startTime + timedelta(seconds=i*period)
    temperature = random.uniform(minTemp, maxTemp)
    humidity = random.uniform(minTemp, maxTemp)
    air = random.uniform(minTemp, maxTemp)
    insertQuery = f"INSERT INTO {tableName} (timestamp, temperature, humidity, air) VALUES (?, ?, ?, ?)"
    cursor.execute(insertQuery, (timestamp.strftime('%Y-%m-%d %H:%M:%S'), temperature, humidity, air))

conn.commit()
conn.close()
