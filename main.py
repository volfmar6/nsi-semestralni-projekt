from flask import Flask, render_template, request, jsonify
import api_routes
import sqlite3
from ReceiveData import thread_read_data

app = Flask(__name__)  # Creating an instance of the Flask class  
app.register_blueprint(api_routes.api_blueprint)

def fetch_data():
    conn = sqlite3.connect('database.db')
    cursor = conn.cursor()

    # předám všechny data 
    cursor.execute('SELECT * FROM data')
    measurements = cursor.fetchall()

    conn.close()  # Close the database connection
    return measurements


@app.route('/')  # View function for endpoint '/'  
def hello():
    return render_template('dashboard.html', measurements=fetch_data())

@app.route('/dashboard') 
def dashboard():  
    return render_template('dashboard.html', measurements=fetch_data())

@app.route('/register') 
def registerpage():  
    return render_template('register.html')  

@app.route('/login/')  
def loginpage():  
    return render_template('login.html')    
 
@app.route('/dashboard/edit')  
def edit():  
    return render_template('dashboardEdit.html',measurements=fetch_data()) 
   
@app.route('/dashboard/filter')    
def filter():  
    return render_template('dashboardFilter.html', measurements=fetch_data())

# Starting a web application at 0.0.0.0.0:5000 with debug mode enabled  
if __name__ == "__main__":  
    thread_read_data()
    app.run(host='0.0.0.0', port=5000, debug=True, use_reloader=True)