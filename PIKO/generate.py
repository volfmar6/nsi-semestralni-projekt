import time
time.sleep(0.1)
from dht import DHT11
from machine import Pin, Timer, ADC
import ujson
from mq135 import MQ135 

air = MQ135(26) 
dht11 = DHT11(Pin(16))  # označení pinů
timer = Timer()
def read_values(timer):
    dht11.measure()
    temperature = dht11.temperature()
    humidity = dht11.humidity()
    corrected_ppm = air.get_corrected_ppm(temperature, humidity)
    D={
        "air":corrected_ppm,
        "temp":temperature,
        "hum":humidity
       }
    print(ujson.dumps(D)) #výpis na sériovou linku
       
timer.init(freq=0.5, mode=Timer.PERIODIC, callback=read_values)


while True:
    pass  