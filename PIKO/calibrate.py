from machine import Pin, Timer, ADC
import ujson


led = Pin('LED', Pin.OUT)
adc = ADC(26)
timer = Timer()

i = 10
mereni= []


def read_values(timer):
    # 10 měření po sobě 
    for _ in range(i):
        air_raw = adc.read_u16() >> 4  # Convert 16-bit ADC value to 12-bit
        air_voltage = (air_raw / 4095.0) * 3.3  # Convert raw value to voltage
        mereni.append(air_voltage)

    # průměr
    average_voltage = sum(mereni) / i

    # Výpis průměrné hodnoty
    print(ujson.dumps({"average_voltage": average_voltage}))

    mereni.clear()

# každých 20 vteřin spustím funkci
timer.init(freq=0.05, mode=Timer.PERIODIC, callback=read_values)

while True:
    pass