import time
from dht import DHT11
from machine import Pin
from mq135 import MQ135 


# setup
air = MQ135(26) # analog PIN 0
dht11 = DHT11(Pin(16)) # D2

# loop
while True:

    dht11.measure()
    temperature = dht11.temperature()
    humidity = dht11.humidity()

    rzero = air.get_rzero()
    corrected_rzero = air.get_corrected_rzero(temperature, humidity)
    resistance = air.get_resistance()
    ppm = air.get_ppm()
    corrected_ppm = air.get_corrected_ppm(temperature, humidity)

    print("DHT11 Temperature: " + str(temperature) +"\t Humidity: "+ str(humidity))
    print("MQ135 RZero: " + str(rzero) +"\t Corrected RZero: "+ str(corrected_rzero)+
          "\t Resistance: "+ str(resistance) +"\t PPM: "+str(ppm)+
          "\t Corrected PPM: "+str(corrected_ppm)+"ppm")
    time.sleep(1)