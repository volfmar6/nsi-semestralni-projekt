jQuery.noConflict();
jQuery(document).ready(function($) {


    //funkce na handle notifikací ---> s proměnnou nepotřebuji pracovat pokud na oznámení neklikám nebo tak
  function showNotification(title, body) {
    
    if (Notification.permission === 'granted') {
        
        var notification = new Notification(title, { body: body });
    }
    }

     //ADD measure form
    $('#add-measure-form').submit(function(event) {
        event.preventDefault();

        var timestamp = $('#timestamp').val();
        var temp = $('#temperature').val();

        $.ajax({
            url: '/api',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify({ timestamp: timestamp, temp: temp }),
            success: function(response) {
            console.log('Measurement created successfully:', response);
                $('#add-measure-form')[0].reset();
                location.reload();
            }
        });
    
    });

    //DELETE measure form 
    $('#delete-measure-form').submit(function(event) {
        event.preventDefault();

        var deleteCount = parseInt($('#deleteCount').val()); //dostanu z inputu value

        $.ajax({
            url: '/api/deleteRange', //zavolám api routu pro deleteCount
            type: 'DELETE',
            data: JSON.stringify({ deleteCount: deleteCount }),
            contentType: 'application/json',
            success: function(response) {
                console.log('Measurement deleted successfully:', response);
                $('#delete-measure-form')[0].reset();
                location.reload(); // Refresh po smazání
            },
            error: function(xhr, status, error) {
                console.error('Error deleting range:', error);
            }

        });
    });

    
    //filter measure form

    $('#filter-measure-form').submit(function(event) {
        event.preventDefault();

        var lastMeasuresCount = parseInt($('#filterCount').val());
        
        //dostanu max ID IDC
        $.ajax({
            url: '/api/maxid',
            type: 'GET',
            success: function(response) {
                // získám požadovaných X záznamů
                $('#measuresContainer').empty();
                
                for (var i = response; i > Math.max(0, response - lastMeasuresCount); i--) {
                    fetchMeasure(i);
                }
            },
            error: function(xhr, status, error) {
                console.error('Error retrieving max ID:', error);
            }
        });
        
    });
    
    function fetchMeasure(measureId) {
        $.ajax({
            url: '/api/' + measureId,
            type: 'GET',
            success: function(response) {
                console.log('dostal jsem data od api_route:',response )
                displayMeasure(response); //zobrazení filtrovaných dat po jednom
                
            },
            error: function(error) {
                console.error('Error retrieving measure:', error);
            }
        });
    }

    function displayMeasure(measure) {
        var measureHtml = 
        '<tr id="measure-' + measure[0]+ '">' +
            '<td>' + measure[0]+'</td>' +
            '<td>' + measure[1] + '</td>' +
            '<td>' + measure[2] + '</td>' +
        '</tr>';

        $('#measuresContainer').append(measureHtml); //zobrazím řádek v tabulce 
    }
    

    $('#register-form').submit(function(event) {
        event.preventDefault();

        var email = $('#email').val();
        var password = $('#password').val();
        var confirmpassword = $('#CPassword').val();
        if(password==confirmpassword)
        {
            var errorMessage = ''
            var succMessage= 'Congratulations on your registration!'
            $('#error-label').text(errorMessage); //vymažu error message
            $('#success-label').text(succMessage); //zobrazím success message

            $.ajax({
                url: '/api/register',
                type: 'POST',
                contentType: 'application/json',
                data: JSON.stringify({ email: email, password: password }),
                success: function(response) {
                console.log('User created successfully:', response);
                    $('#register-form')[0].reset();
                    window.location.href = 'login';  //přesměrování na login page 
                }
            });
        
        }
        else
        {
            var errorMessage = 'Zadaná hesla se neshodují'
            var succMessage= ''
            $('#error-label').text(errorMessage); //zobrazím error message
            $('#success-label').text(succMessage); //zobrazím success message
        }
        
    });

    $('#login-form').submit(function(event) {
        event.preventDefault();

        var email = $('#email').val();
        var password = $('#password').val();

        $.ajax({
            url: '/api/login',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify({ email: email, password: password }),
            success: function(response) {
            console.log('User logon successfully:', response);
                $('#login-form')[0].reset();
                localStorage.setItem('username', email);
                window.location.href = '/dashboard';
            }
        });     
    });
    $(document).ready(function() {                              //fix na mizení hodnot ... incializace full reloadu nechá zmizet hodnoty
        var storedUsername = localStorage.getItem('username');
        if (storedUsername) {
            $('#username_nav').text('Welcome back, ' + storedUsername + '!');
            $('#logout-button').show();
        }
        else
        {
            
        }
    });

    $('#logout-button').click(function() {  //signout

        localStorage.removeItem('username');
        $('#username-display').text('');
        $('#logout-button').hide();
        location.reload();
    });

    //GRAFY ---------------------------------------------------------------------------------
    const tempCtx = document.getElementById('temperatureChart').getContext('2d');
    const humiCtx = document.getElementById('humidityChart').getContext('2d');
    const airCtx = document.getElementById('airqualityChart').getContext('2d');

    const source = new EventSource('/stats');
            source.onmessage = function (event) {
                const data = JSON.parse(event.data);
                
                addData(temperatureChart, data);
                addData(humidityChart, data);
                addData(airChart, data);
            };
    const tempData = { //style line
        labels: [],
        datasets: [{
            label: 'Temperature',
            borderColor: 'rgba(75, 192, 192, 1)',
            backgroundColor: 'rgba(75, 192, 192, 0.2)',
            data: []
        }]
    };

    const humiData = {
        labels: [],
        datasets: [{
            label: 'Humidity',
            borderColor: 'rgba(255, 99, 132, 1)',
            backgroundColor: 'rgba(255, 99, 132, 0.2)',
            data: []
        }]
    };

    const airData = {
        labels: [],
        datasets: [{
            label: 'Air',
            borderColor: 'rgba(54, 162, 235, 1)',
            backgroundColor: 'rgba(54, 162, 235, 0.2)',
            data: []
        }]
    };
    //nastavení grafů ---> osy,rozlišení
    const temperatureChart = new Chart(tempCtx, { 
        type: 'line',
        data: tempData,
        options: {
            responsive: true,
            scales: {
                x: {
                    type: 'time',
                    time: {
                        unit: 'hour',
                        tooltipFormat: 'HH:mm:ss',
                        displayFormats: {
                            hour: 'HH:mm'
                        }
                    }
                },
                y: {
                    min: 10,
                    max: 30,
                    title: {
                        display: true,
                        text: 'Temperature [°C]'
                    }
                }
            }
    
        }
    });

    const humidityChart = new Chart(humiCtx, {
        type: 'line',
        data: humiData,
        options: {
            responsive: true, 
            scales: {
                x: {
                    type: 'time',
                    time: {
                        unit: 'hour',
                        tooltipFormat: 'HH:mm:ss',
                        displayFormats: {
                            hour: 'HH:mm'
                        }
                    }
                },
                y: {
                    min: 20,
                    max: 90,
                    title: {
                        display: true,
                        text: 'Humidity [%]'
                    }
                }
            }
        }
    });

    const airChart = new Chart(airCtx, {
        type: 'line',
        data: airData,
        options: {
            responsive: true,
            scales: {
                x: {
                    type: 'time',
                    time: {
                        unit: 'hour',
                        tooltipFormat: 'HH:mm:ss',
                        displayFormats: {
                            hour: 'HH:mm'
                        }
                    }
                },
                y: {
                    min: 400,
                    max: 10000,
                    title: {
                        display: true,
                        text: 'C02 concetration [ppm]'
                    }
                }
            }
        }
    });  

    function addData(chart, data) {
        chart.data.labels = data.map(entry => entry.timestamp);
        chart.data.datasets[0].data = data.map(entry => entry[chart.data.datasets[0].label.toLowerCase()]);
        chart.update();
    }
    //NOTIFIKACE------------------------------------------------------------------------------------------

    
    if ('Notification' in window) {
        
        Notification.requestPermission().then(function (permission) { //povolení v browseru
            if (permission === 'granted') {
                showNotification('Air Quality Alert', 'Air quality exceeds 1500 ppm!');
            }
        });
        
    }
});

