from flask import request, jsonify, Blueprint, Response
import sqlite3, rsa, random,time,json
api_blueprint = Blueprint('api', __name__)


# Load keys from files
with open('public.pem', 'rb') as public_file:
    publicKey = rsa.PublicKey.load_pkcs1(public_file.read())
with open('private.pem', 'rb') as private_file:
    privateKey = rsa.PrivateKey.load_pkcs1(private_file.read())

def generate_data():
    while True:
        conn = sqlite3.connect('database.db')
        cursor = conn.cursor()
        cursor.execute('SELECT timestamp, temperature, humidity, air FROM data ORDER BY timestamp DESC LIMIT 200')
        measures = cursor.fetchall()
        conn.close()
        data = [{"timestamp": measure[0], "temperature": measure[1], "humidity": measure[2], "air": measure[3]}
                 for measure in measures]
        yield f"data: {json.dumps(data)}\n\n"
        time.sleep(1)



@api_blueprint.route('/stats')
def stats():
    return Response(generate_data(), mimetype='text/event-stream')
        
@api_blueprint.route('/api', methods=['GET'])        #funkce pro vydání všech dat                         
def get_measures():
    conn = sqlite3.connect('database.db') #connect na database.db
    cursor = conn.cursor()

    cursor.execute('SELECT * FROM data')
    measurements = cursor.fetchall()
    conn.close() 

    return jsonify(measurements)

@api_blueprint.route('/api', methods=['POST'])         #tvorba dat                        
def create_measure():
    conn = sqlite3.connect('database.db')
    cursor = conn.cursor()

    new_measure = request.json
    timestamp = new_measure.get('timestamp')
    temperature = new_measure.get('temp')
    print('timestamp:',timestamp)
    cursor.execute('INSERT INTO data (timestamp, temperature) VALUES (?, ?)', (timestamp, temperature)) # insert dat do databáze
    conn.commit()
    cursor.close()

    return jsonify({"message": "Measurement created successfully"}), 201
    

@api_blueprint.route('/api/maxid', methods=['GET'])     #maxid return funkce
def get_maxid():
    conn = sqlite3.connect('database.db')
    cursor = conn.cursor()

    cursor.execute('SELECT MAX(id) FROM data')
    maxid = cursor.fetchone()[0] # jeden řádek sloupec [0]---> id
    conn.close()
    print('maxid==:',maxid)
    return jsonify(maxid)
    
@api_blueprint.route('/api/<int:measure_id>', methods=['GET'])        # vrátí pouze measure podle id pokud existuje, pokud ne, vrátí measure not found
def get_measure(measure_id):
    conn = sqlite3.connect('database.db') #connect na database.db
    cursor = conn.cursor()
    cursor.execute("""SELECT id, timestamp, temperature
                    FROM data 
                   WHERE id=?""", (measure_id,))
    measure=cursor.fetchone()
    conn.close()
    if measure:
        print("Found measure :",measure_id)
        return jsonify(measure)
    else:
        return jsonify({"message": "Measurement not found"}), 404


@api_blueprint.route('/api/deleteRange', methods=['DELETE'])                             #route pro smazání více položek najednou
def delete_measure_range():
    conn = sqlite3.connect('database.db') 
    cursor = conn.cursor()

    delete_count = request.json.get('deleteCount', 0)
    if delete_count > 0:
        cursor.execute("""DELETE FROM data WHERE id IN (
                          SELECT id FROM data LIMIT ?)""", (delete_count,))  #vyberu si vrchní data limitem od delete count -> pokud posílám parametr musí být zapsán takhle
        conn.commit()
        conn.close()
        
        return jsonify({"message": f"Deleted first {delete_count} measurements"}), 200
    else:
        return jsonify({"error": "Invalid delete count"}), 400
 
@api_blueprint.route('/api/register', methods=['POST'])         #tvorba uživatele                        
def create_user():
    conn = sqlite3.connect('database.db')
    cursor = conn.cursor()

    new_user = request.json
    email = new_user.get('email')
    password = new_user.get('password')
    encrypt_password=rsa.encrypt(password.encode('utf8'), publicKey)
    print('email:',email)
    print('password',password)
    print('encrypted password',encrypt_password)
    cursor.execute('INSERT INTO users (email,password) VALUES (?, ?)', (email, encrypt_password)) # insert dat do databáze
    conn.commit()
    conn.close()

    return jsonify({"message": "User created successfully"}), 201

@api_blueprint.route('/api/login', methods=['POST'])         #tvorba uživatele                        
def login_user():
    conn = sqlite3.connect('database.db')
    cursor = conn.cursor()

    user = request.json
    email = user.get('email')
    password = user.get('password')
    #print('email:',email)
    #print('password',password)
    cursor.execute('SELECT * FROM users WHERE email=?', (email,)) # vyčtu data z databáze
    validation_user=cursor.fetchone()
    if validation_user:
        conn.commit()
        cursor.close()
        db_password=validation_user[2]
        #print(db_password)
        decrypt_pass=rsa.decrypt(db_password, privateKey).decode('utf8')
        print('decoded pass from db:',decrypt_pass)
        if password==decrypt_pass:
            return jsonify(email), 200
        else:
            return jsonify({"message": "Invalid credentials"}), 401
    else:
        return jsonify({"error": "User not found"}), 404
