import serial
import sqlite3
import json
import datetime
import threading

def adapt_datetime_iso(val):
    """Adapt datetime.datetime to timezone-naive ISO 8601 date."""  #adapter na datetime.datetime 
    return val.isoformat()
sqlite3.register_adapter(datetime.datetime, adapt_datetime_iso) 

def read_data():
    ser = serial.Serial('COM3', 115200)  #připojení k UART
    conn = sqlite3.connect('database.db') #připojení k databázi
    cursor = conn.cursor()
    try:
        while True:
            
            line = ser.readline().decode().strip()
            print("Received: ", line)  
            data = json.loads(line)
            temp = data["temp"] #odjasonuju
            hum = data["hum"]
            air = data["air"]
                
            cursor.execute("INSERT INTO data (timestamp, temperature, humidity, air) VALUES (?, ?, ?, ?)", ( sqlite3.datetime.datetime.now(),temp,hum,air)) #přidám záznam do databáze
            conn.commit()
    except KeyboardInterrupt:
        ser.close()
        conn.close()  

def thread_read_data():                                    #vytvářím pro tohle thread aby mohla běžet flask aplikace zároveň
    serial_thread = threading.Thread(target=read_data)
    serial_thread.daemon = True  # pokud vypnu program vypnu i tenhle thread
    serial_thread.start()